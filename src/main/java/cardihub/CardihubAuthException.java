package cardihub;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class CardihubAuthException extends RuntimeException {

    private static final long serialVersionUID = -190268011826494319L;

	public CardihubAuthException(String exception) {
        super(exception);
    }

}