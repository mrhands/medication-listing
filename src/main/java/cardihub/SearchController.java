package cardihub;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import cardihub.medicine.MedicineClient;

@RestController
public class SearchController {

    private final MedicineClient medicineClient;

    @Autowired
    public SearchController(final MedicineClient medicineClient) {
        this.medicineClient = medicineClient;
    }

    @GetMapping("/search_medication")
    public List<Medicine> searchMedication(@RequestParam(value="username") String username,
                                            @RequestParam(value="password", defaultValue="World") String password,
                                            @RequestParam(value="medication_list") String medicationList) {

        UserCredentials credentials = new UserCredentials(username, password);
        medicineClient.configure(credentials);

        return medicineClient.fetchMedicineList(medicationList);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public final ResponseEntity<ErrorDetails> handleMissingParams(MissingServletRequestParameterException ex, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
            request.getDescription(false));
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

}
