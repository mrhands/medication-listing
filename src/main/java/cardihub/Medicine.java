package cardihub;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Medicine {

    private String id;
    private String displayName;
    private String appearance;
    private String doseType;

	public Medicine(@JsonProperty("id") String id,
					@JsonProperty("displayName") String displayName,
					@JsonProperty("appearance") String appearance,
					@JsonProperty("doseType") String doseType) {
        this.id = id;
        this.displayName = displayName;
        this.appearance = appearance;
        this.doseType = doseType;
    }

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the appearance
	 */
	public String getAppearance() {
		return appearance;
	}

	/**
	 * @param appearance the appearance to set
	 */
	public void setAppearance(String appearance) {
		this.appearance = appearance;
	}

	/**
	 * @return the doseType
	 */
	public String getDoseType() {
		return doseType;
	}

	/**
	 * @param doseType the doseType to set
	 */
	public void setDoseType(String doseType) {
		this.doseType = doseType;
	}

}
