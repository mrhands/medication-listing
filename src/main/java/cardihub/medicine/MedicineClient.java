package cardihub.medicine;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import cardihub.CardihubAuthException;
import cardihub.CardihubNotFoundException;
import cardihub.Medicine;
import cardihub.UserCredentials;

@Service
public class MedicineClient {

    private static final String AUTH_URI = "login";
    private static final String SEARCH_MEDICINE_URI = "medications/search";
    private final RestTemplate restTemplate;
    private final String medicineServiceUrl;
    private String medicineServiceApiToken;

    @Autowired
    public MedicineClient(final RestTemplate restTemplate, 
                        @Value("${cardihub.url}") final String medicineServiceUrl) {
        this.restTemplate = restTemplate;
        this.medicineServiceUrl = medicineServiceUrl;
    }

    public void configure(final UserCredentials userCredentials) {
        this.medicineServiceApiToken = fetchAuthToken(userCredentials);
    }

    public String getMedicineServiceApiToken() {
        return this.medicineServiceApiToken;
    }

    public String fetchAuthToken(UserCredentials userCredentials) {
        String url = String.format("%s/%s", medicineServiceUrl, AUTH_URI);
        try {
            String authToken = restTemplate.postForObject(url, userCredentials, String.class);

            // Response token has quotes.
            // Remove quotes from the token
            return authToken.replace("\"", "");
        } catch(HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.FORBIDDEN)
                throw new CardihubAuthException(String.format("Authentication Failed for username %s", userCredentials.getUsername()));
            throw e;
        }
    }

    public List<Medicine> fetchMedicineList(String medicationList) {
        String url = String.format("%s/%s?count=100&term={term}", medicineServiceUrl, SEARCH_MEDICINE_URI);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", this.medicineServiceApiToken);
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<List<Medicine>> response = restTemplate.exchange(url, HttpMethod.GET, entity, new ParameterizedTypeReference<List<Medicine>>() {}, medicationList);
            return response.getBody();
        } catch(HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.FORBIDDEN)
                throw new CardihubAuthException("Auth token is invalid");
            if (e.getStatusCode() == HttpStatus.NOT_FOUND)
                throw new CardihubNotFoundException(String.format("Cannot find matches for '%s'", medicationList));
            throw e;
        }
    }
}
