package cardihub;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import cardihub.medicine.MedicineClient;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = SearchController.class)
public class SearchControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MedicineClient medicineClient;

    @Test
    public void shouldReturnMedicineList() throws Exception {
        List<Medicine> medicineList = new ArrayList<Medicine>();
        medicineList.add(new Medicine("1", "display", "appearance", "dose"));

        given(medicineClient.fetchMedicineList("test")).willReturn(medicineList);
        mockMvc.perform(get("/search_medication?username=testuser&password=test123&medication_list=test"))
            .andExpect(status().is2xxSuccessful())
            .andExpect(content().string("[{\"id\":\"1\",\"displayName\":\"display\",\"appearance\":"
            + "\"appearance\",\"doseType\":\"dose\"}]"));
    }

}
