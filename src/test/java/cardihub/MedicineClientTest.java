package cardihub;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import cardihub.medicine.MedicineClient;

public class MedicineClientTest {

    private MedicineClient subject;

    @Mock
    private RestTemplate restTemplate;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        subject = new MedicineClient(restTemplate, "http://localhost:8080");
    }

    @Test
    public void shouldCallMedicineAuth() throws Exception {
        String authToken = "1234";
        UserCredentials credentials = new UserCredentials("username", "password");
        given(restTemplate.postForObject("http://localhost:8080/login", credentials, String.class))
            .willReturn(String.format("\"%s\"", authToken));
        
        subject.configure(credentials);

        assertEquals(subject.getMedicineServiceApiToken(), authToken);
    }

    @Test(expected = CardihubAuthException.class)
    public void shouldFailAuth() throws Exception {
        String authToken = "1234";
        UserCredentials credentials = new UserCredentials("username", "password");
        given(restTemplate.postForObject("http://localhost:8080/login", credentials, String.class))
            .willThrow(new HttpClientErrorException(HttpStatus.FORBIDDEN));
        
        subject.configure(credentials);
    }

    @Test
    public void shouldReturnMedicineList() throws Exception {
        String authToken = "1234";
        String term = "medicine";
        UserCredentials credentials = new UserCredentials("username", "password");

        List<Medicine> medicineList = new ArrayList<Medicine>();
        medicineList.add(new Medicine("1", "display", "appearance", "dose"));
        ResponseEntity<List<Medicine>> response = new ResponseEntity<List<Medicine>>(medicineList, HttpStatus.OK);

        given(restTemplate.postForObject("http://localhost:8080/login", credentials, String.class))
            .willReturn(String.format("\"%s\"", authToken));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", authToken);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        given(restTemplate.exchange("http://localhost:8080/medications/search?count=100&term={term}",
            HttpMethod.GET, entity, new ParameterizedTypeReference<List<Medicine>>() {}, term))
            .willReturn(response);
        
        subject.configure(credentials);
        assertEquals(medicineList, subject.fetchMedicineList(term));
    }

    @Test(expected = CardihubNotFoundException.class)
    public void shouldReturnNotFound() throws Exception {
        String authToken = "1234";
        String term = "medicine";
        UserCredentials credentials = new UserCredentials("username", "password");

        given(restTemplate.postForObject("http://localhost:8080/login", credentials, String.class))
            .willReturn(String.format("\"%s\"", authToken));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", authToken);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        given(restTemplate.exchange("http://localhost:8080/medications/search?count=100&term={term}",
            HttpMethod.GET, entity, new ParameterizedTypeReference<List<Medicine>>() {}, term))
            .willThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));

        subject.configure(credentials);
        subject.fetchMedicineList(term);
    }
}
