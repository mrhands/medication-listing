:spring_version: current
:toc:
:project_id: gs-rest-service
:spring_version: current
:spring_boot_version: 2.0.2.RELEASE
:icons: font
:source-highlighter: prettify

An API implementation that connects to the Cardihub service to get medication listing

== Running a test server

For linux, we run (at least my preferred way):

----
./gradlew runBoot
----

To run tests

----
./gradlew test
----
